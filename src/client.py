import socket
import select
import sys

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
if len(sys.argv) != 4:
    print("Correct usage: client.py <username> <host> <port>")
    exit()
username = str(sys.argv[1])
host = str(sys.argv[2])
port = int(sys.argv[3])
server.connect((host, port))

server.send(username.encode('utf-8'))

greetings = server.recv(2048).decode("utf-8")
print(greetings)

while True:
    sockets_list = [sys.stdin, server]
    read_sockets, write_socket, error_socket = select.select(sockets_list, [], [])
    quit = False
    for socks in read_sockets:
        if socks == server:
            entire = socks.recv(100000).decode("utf-8")
            splits = entire.split(":")
            type = splits[0]


            if(type == "init"):
                greetings = splits[1]
                print(greetings)
                num_existing_messages = int(splits[2])
                for i in range(round(num_existing_messages)):
                    existing_username = splits[3 + (i * 2)]
                    if(username == existing_username):
                        existing_username = "You"
                    message = splits[3 + (i * 2) + 1]
                    print("<" + existing_username + "> " + message)
            else:
                message = splits[1]
                print(message)
        else:
            message = sys.stdin.readline()
            if(message.strip() == '/quit'):
                quit = True
                break
            if(message.strip() == '/init'):
                server.send("init".encode('utf-8'))
                break
            server.send(("message:" + username + ":" + message).encode('utf-8'))
            sys.stdout.write("<You> ")
            sys.stdout.write(message)
            sys.stdout.flush()
    if(quit):
        break
server.close()