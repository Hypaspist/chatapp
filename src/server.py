import os
import socket
import sys
import select
import sqlalchemy
from _thread import *
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import sessionmaker
import datetime

if len(sys.argv) != 4:
    print("Correct usage: server.py <name> <host> <port>")
    exit()
name = str(sys.argv[1])
host = str(sys.argv[2])
port = int(sys.argv[3])

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind((host, port))
server.listen(1000)

clients = []

engine = create_engine('sqlite:///chat.db', echo=True)
Base = declarative_base()
class Message(Base):
    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True)
    message = Column(String)
    username = Column(String)
    created_date = Column(DateTime, default=datetime.datetime.utcnow)
Base.metadata.create_all(engine)



def clientthread(conn, addr):

    while True:
        try:
            entire = conn.recv(100000).decode("utf-8")
            splits = entire.split(":")
            type = splits[0]
            if(type == "init"):
                Session = sessionmaker(bind=engine)
                init_session = Session()
                existing_messages = init_session.query(Message).all()
                init_session.flush()
                num_existing_messages = str(len(existing_messages))
                message_string = "init:" + "Welcome to the " + name + " chatroom!"
                message_string += ":" + num_existing_messages
                for existing_message in existing_messages:
                    message_string +=":" + existing_message.username
                    message_string +=":" + existing_message.message
                conn.send(message_string.encode('utf-8'))
            else:
                username = splits[1]
                message = splits[2]

                Session = sessionmaker(bind=engine)
                session = Session()
                entry = Message(message=message, username=username)
                session.add(entry)
                session.commit()

                if message:
                    print("<" + username + "> " + message)
                    message_to_send = "<" + username + "> " + message
                    broadcast(message_to_send, conn)
                else:
                    remove(conn)
        except:
            continue

def broadcast(message, connection):
    for client in clients:
        if client != connection:
            try:
                client.send(("message:" + message).encode("utf-8"))
            except:
                client.close()
                remove(client)

def remove(connection):
    if connection in clients:
        clients.remove(connection)

while True:
    conn, addr = server.accept()
    clients.append(conn)
    username = conn.recv(2048).decode('utf-8')
    print(username + " has connected")
    conn.send(("Welcome to the " + name + " chatroom!").encode('utf-8'))

    start_new_thread(clientthread, (conn, addr))

conn.close()
server.close()
